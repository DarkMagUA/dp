-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Трв 09 2021 р., 19:54
-- Версія сервера: 10.4.11-MariaDB
-- Версія PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `pf_sell_car`
--

-- --------------------------------------------------------

--
-- Структура таблиці `application`
--

CREATE TABLE `application` (
  `id` int(11) UNSIGNED NOT NULL,
  `date` datetime NOT NULL,
  `id_user` int(11) UNSIGNED NOT NULL,
  `descript` text NOT NULL,
  `status` enum('draft','publiched','canseled','active','closed') NOT NULL,
  `commit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `branches`
--

CREATE TABLE `branches` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_company` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` int(16) NOT NULL,
  `country` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `id_commit` int(11) NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `cars_available`
--

CREATE TABLE `cars_available` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_user` int(11) UNSIGNED NOT NULL,
  `id_car` int(11) UNSIGNED NOT NULL,
  `status` enum('available','not available','sold') NOT NULL,
  `date` datetime NOT NULL,
  `price` double NOT NULL,
  `commit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `di`
--

CREATE TABLE `di` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_country` int(11) NOT NULL,
  `city` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `dictionary_car`
--

CREATE TABLE `dictionary_car` (
  `id` int(11) UNSIGNED NOT NULL,
  `name_car` varchar(50) NOT NULL,
  `model_car` varchar(50) NOT NULL,
  `commit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `dictionary_country`
--

CREATE TABLE `dictionary_country` (
  `id` int(11) UNSIGNED NOT NULL,
  `country` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `group_user`
--

CREATE TABLE `group_user` (
  `id` int(11) NOT NULL COMMENT 'ID користувача',
  `group` varchar(50) NOT NULL COMMENT 'тип користувача'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `group_user`
--

INSERT INTO `group_user` (`id`, `group`) VALUES
(1, 'superadmin'),
(2, 'member'),
(3, 'company'),
(4, 'client');

-- --------------------------------------------------------

--
-- Структура таблиці `status_uers`
--

CREATE TABLE `status_uers` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_status` int(11) UNSIGNED NOT NULL,
  `pri` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `status_user`
--

CREATE TABLE `status_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_who_write` int(11) UNSIGNED NOT NULL,
  `id_when` int(11) UNSIGNED NOT NULL,
  `data` datetime NOT NULL,
  `status` enum('Positiv','Negativ','Neitral','') NOT NULL,
  `commit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_user` int(11) UNSIGNED NOT NULL,
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_city` int(11) UNSIGNED NOT NULL,
  `id_status` int(11) UNSIGNED NOT NULL,
  `id_commit` int(11) UNSIGNED NOT NULL,
  `name_company` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `zzz`
--

CREATE TABLE `zzz` (
  `id` int(11) UNSIGNED NOT NULL,
  `login` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `id_group` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `cars_available`
--
ALTER TABLE `cars_available`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `di`
--
ALTER TABLE `di`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `dictionary_car`
--
ALTER TABLE `dictionary_car`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `dictionary_country`
--
ALTER TABLE `dictionary_country`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `group_user`
--
ALTER TABLE `group_user`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `status_uers`
--
ALTER TABLE `status_uers`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `status_user`
--
ALTER TABLE `status_user`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `zzz`
--
ALTER TABLE `zzz`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `application`
--
ALTER TABLE `application`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `cars_available`
--
ALTER TABLE `cars_available`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `di`
--
ALTER TABLE `di`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `dictionary_car`
--
ALTER TABLE `dictionary_car`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `dictionary_country`
--
ALTER TABLE `dictionary_country`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `group_user`
--
ALTER TABLE `group_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID користувача', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблиці `status_uers`
--
ALTER TABLE `status_uers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `status_user`
--
ALTER TABLE `status_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `zzz`
--
ALTER TABLE `zzz`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
